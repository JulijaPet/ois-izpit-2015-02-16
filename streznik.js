var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});



app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});


app.get('/api/dodaj', function(req, res) {
	var davcnaSt = req.query.ds;
	var ime = req.query.ime;
	var priimek = req.query.priimek;
	var ulica = req.query.ulica;
	var hisnaStevilka = req.query.hisnaStevilka;
	var postnaStevilka = req.query.postnaStevilka;
	var kraj = req.query.kraj;
	var drzava = req.query.drzava;
	var poklic = req.query.poklic;
	var telefonskaStevilka = req.query.telefonskaStevilka;
	var obstaja = false;
	for(var i in uporabnikiSpomin) {
		var davSt = uporabnikiSpomin[i]["davcnaStevilka"];
		if(davSt == davcnaSt) {
			obstaja = true;
		}
	}
	if(davcnaSt == null || davcnaSt.length == 0 || ime == null || ime.length == 0 || priimek == null || priimek.length == 0 || ulica == null || ulica.length == 0 || hisnaStevilka == null || hisnaStevilka.length == 0 || postnaStevilka== null || postnaStevilka.length == 0 || drzava == null || drzava.length == 0 || poklic == null || poklic.length == 0 || telefonskaStevilka == null || telefonskaStevilka.length == 0 || kraj == null || kraj.length == 0)
		res.send("Napaka pri dodajanju osebe!");
	else if(obstaja)
		res.send("Oseba z davčno številko " + davcnaSt + " že obstaja!<br/><a href='javascript:window.history.back()'>Nazaj</a>");
	else {
		var new_obj = {"davcnaStevilka": davcnaSt, "ime": ime, "priimek": priimek, "naslov": ulica, "hisnaStevilka": hisnaStevilka, "postnaStevilka": postnaStevilka, "kraj": kraj, "drzava": drzava, "poklic": poklic, "telefonskaStevilka": telefonskaStevilka};
		uporabnikiSpomin.push( new_obj );
	}
});


app.get('/api/brisi', function(req, res) {
	var davcnaSt = req.query.id;
	if(davcnaSt == null || davcnaSt.length == 0)
		 res.send("Napačna zahteva!");
	var obstaja = false;
	for(var i in uporabnikiSpomin) {
		var davSt = uporabnikiSpomin[i]["davcnaStevilka"];
		if(davSt == davcnaSt) {
			obstaja = true;
			res.send(uporabnikiSpomin.splice(i, 1));
			res.redirect('/');
			
		}
	}
	if(!obstaja)
		res.send("Oseba z davčno številko " + davcnaSt + " ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>");
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var uporabnikiSpomin = [
	{davcnaStevilka: '98765432', ime: 'James', priimek: 'Blond', naslov: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'}, 
	{davcnaStevilka: '12345678', ime: 'Ata', priimek: 'Smrk', naslov: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];